# Data Viz
## Il s'agit d'un projet de Data Viz écrit en Python qui analyses les éléctions présidentielles de 2012
### [Télécharger le fichier zip data-viz :inbox_tray:](https://github.com/pzim-devdata/DATA-developer/releases/download/V1.0.0/data-viz.zip)

![Consulter le notebook Python](Projet%20%C3%A9l%C3%A9ctions.pdf):blue_book:

[Télécharger le notebook Python :inbox_tray:](https://github.com/pzim-devdata/DATA-developer/raw/master/data-viz/Projet%20%C3%A9l%C3%A9ctions.pdf)


Le projet consiste à :

- Nettoyer et ordonner des données avec Python importées depuis des sources Internet comme l'INSEE
- Créer de la DataViz avec les libraires matplotlib matplotlib, pandas et numpy
- Créer une carte interative avec folium
- Etudier les corrélations avec seaborn
- Rendre les données exploitables pour un Data Anlayst à travers une application Flask


----------------------------------

Répartition des votes au second tour par départments :

![alt text](static/Repartition%20votes%20deuxieme%20tour.png)

----------------------------------

Répartition des votes au second tour par région :

![alt text](static/Repartition%20votes%20deuxieme%20tour%20region.png)

----------------------------------

Correlations entre votes et chômage au deuxiéme tour :

![alt text](static/Correlations%20entre%20votes%20et%20ch%C3%B4mage%20deuxieme%20tour.png)

----------------------------------

Correlations des votes entre le premier tour et le second tour pour Hollande et Sarkozy :

![alt text](static/Corr%C3%A9lation%20des%20votes%20entre%20le%20premier%20tour%20et%20le%20second%20tour%20pour%20Hollande%20et%20Sarkozy.png)

----------------------------------

Réparition des votes au premier tour et taux de chomage par département :

![alt text](static/R%C3%A9parition%20des%20votes%20au%20premier%20tour%20et%20taux%20de%20chomage%20par%20d%C3%A9partement%20(en%20_).png)

----------------------------------

[Plus de graphes](static)
