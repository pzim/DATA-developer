# Projets effectués en tant que Dev Python DATA :
- [Chef-d-oeuvre](#-chef-d-oeuvre)
- [Work experience in a company](#-work-experience)
- [Data Viz](#-data-viz)
- [Creation of a huge SQL database](#-creation-of-a-huge-sql-database)
- [Other projects](#-other-projects)


[Mes attestations et diplômes de formation 🎓](https://gitlab.com/pzim/Skills-and-training-certificates)


## - [Chef-d-oeuvre](chef-d'oeuvre)
Il s'agit du projet de fin de formation utilisant Python, MongoDB et MySQL

[Télécharger le projet :inbox_tray:](https://github.com/pzim-devdata/dev-data/raw/master/chef-d'oeuvre/chef_d_oeuvre.zip)

Voici le rapport du projet :

![Consulter le rapport ](https://gitlab.com/pzim/DATA-developer/-/blob/master/chef-d'oeuvre/Rapport.pdf):blue_book:

[Télécharger le rapport :inbox_tray:](https://github.com/pzim-devdata/dev-data/raw/master/chef-d'oeuvre/Rapport.pdf)


Le projet consiste à :

- Scraper sur Wikipédia l'ensemble des villes d'île de France, leurs code postaux et leurs coordonnées (latitude, longitude)
- Scraper grâce à l'API Google Maps les temps de parcours et distances vers Paris en transport en commun, en voiture aux heures de pointes et en trafic normal et en vélo
- Sauvegarder les données non expolitées sur mySQL dans differentes tables, par département, avec en clé étrangère le nom des villes
- Nettoyer les données pour les expoliter et créer de la DataViz pour en dégager une problématique
- Sauvegarder les données assemblées et nettoyées sur MongoDB

--------------------------------------------

## - [Work experience](work%20experience)
Il s'agit des programmes effectués durant mon stage en entreprise

[Télécharger tout le projet :inbox_tray:](https://github.com/pzim-devdata/DATA-developer/releases/download/V1.0.0/work-experiencer.zip)

Voici les 3 programmes effectués durant le stage :

- [scrape-iscid-master](work%20experience/scrape-icsid-master)
- [scrape-pca-master](work%20experience/scrape-pca-master)
- [scrape-unctad-master](work%20experience/scrape-unctad-master)

Le projet consiste à :
- Scraper trois sites Internet avec BeautifullSoup et Selenium
- Repérer si ces sites ont été modifiés grâce à des sauvegardes en CSV 
- Mettre en forme ces modifications afin de les notifier par mail pour les integrer dans une base de données

--------------------------------------------

## - [Data Viz](data-viz)
Il s'agit d'un projet de Data Viz écrit en Python qui analyses les éléctions présidentielles de 2012

[Télécharger tout le projet :inbox_tray:](https://github.com/pzim-devdata/DATA-developer/releases/download/V1.0.0/data-viz.zip)

![Consulter le notebook Python](https://gitlab.com/pzim/DATA-developer/-/blob/master/data-viz/Projet%20%C3%A9l%C3%A9ctions.pdf):blue_book:

[Télécharger le notebook Python:inbox_tray:](https://github.com/pzim-devdata/DATA-developer/raw/master/data-viz/Projet%20%C3%A9l%C3%A9ctions.pdf)


Le projet consiste à :

- Nettoyer et ordonner des données avec Python importées depuis des sources Internet comme l'INSEE
- Créer de la DataViz avec les libraires matplotlib, pandas et numpy
- Créer une carte interative avec folium
- Etudier les corrélations avec seaborn
- Rendre les données exploitables pour un Data Anlayst à travers une application Flask

--------------------------------------------

## - [Creation of a huge SQL database](creation%20of%20a%20huge%20SQL%20database)
Il s'agit de créer une base de données SQL pour une chaine commerciale avec un grand nombre de données 

[Télécharger le projet :inbox_tray:](https://github.com/pzim-devdata/DATA-developer/releases/download/V1.0.0/creation.of.a.huge.SQL.database.zip)

Le projet consiste à :
- Créer un script pour le schéma avec 9 tables en se basant sur un MCD
- Créer un script pour les constraintes et les clés étrangéres pour s'assurer de la cohérence des donnés
- Générer des données aléatoires (je me suis aidé du site : https://www.generatedata.com/#generator qui permet de créer toutes sortes de données)
- Nettoyer les données
- Créer un script pour insérer un grand nombre de données (10888 lignes)
- Créer un script de requêtes pour tester la base de données
- Sauvegarder la base de données

--------------------------------------------

## - [Other projects](https://drive.google.com/open?id=1cQq2cGvo1ENwOm2BFHG41D25A-qA1RSS)
Vous pouvez consulter la totalité des travaux sur mon [Drive :blue_book:](https://drive.google.com/open?id=1cQq2cGvo1ENwOm2BFHG41D25A-qA1RSS)

Vous y trouverez :
- Les TP
- Les veilles
- Les projets en groupe






